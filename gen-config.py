#!/usr/bin/env python3
import argparse
from ase.io import write, read
from ase import Atoms
from ase.lattice.cubic import FaceCenteredCubic
from io import StringIO

parser = argparse.ArgumentParser(description='Generate an LiF FCC configuration')

parser.add_argument('-cells', dest='cells', type=int, default=5, help='FCC unit cell repetition in each direction (x,y,z)')
parser.add_argument('-o', dest='file', type=str, default='LiF.cif', help='Output filename')
parser.add_argument('-lattice_constant', dest='lattice_constant', type=float, default=3.5, help='FCC lattice constant')
args = parser.parse_args()

atoms = FaceCenteredCubic(directions=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
                          symbol='Ar',
                          size=(args.cells, args.cells, args.cells),
                          latticeconstant=args.lattice_constant,
                          pbc=True)

for i in range(len(atoms)):
    if(i % 2 == 0):
        atoms[i].symbol = 'Li'
    else:
        atoms[i].symbol = 'F'

write(args.file,atoms,format='cif')
