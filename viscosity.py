import numpy as np
import argparse
from typing import Iterable
from tqdm import tqdm
import matplotlib.pyplot as plt
from pathlib import Path

def integrate(x: Iterable[float], dt: float) -> float:
    return np.sum(x*dt)

def correlate(x: Iterable[float], y: Iterable[float]) -> Iterable[float]:
    if not (len(x) == len(y)):
        raise Exception("Arrays do not have the same size")
    X = np.concatenate((x, np.zeros(len(x)-1)))
    Y = np.concatenate((y, np.zeros(len(y)-1)))
    fft_X = np.fft.fft(X)
    fft_Y = np.fft.fft(Y)
    cor = np.fft.ifft(np.conj(fft_X)*fft_Y)
    cor = cor[:len(cor)//2+1]
    return cor / [cor.shape[0]-j for j in range(cor.shape[0])]

parser = argparse.ArgumentParser(description='Calculate Viscosity from a Janus run')

parser.add_argument('--stats-path', action='store', dest='path', type=str, required=True, help="path to the stats file")
parser.add_argument('-out', action='store', dest='out', type=str, default=None, help="path to store results")
parser.add_argument('--max-lag', action='store', dest='lags', type=int, default=100)
parser.add_argument('-show', action='store_true')

args = parser.parse_args()
if args.out is None:
    out = '/'.join(args.path.split('/')[0:-1])

stats = np.loadtxt(args.path)

# Step | Real Time [s] | Time [fs] | Epot/N [eV] | Ekin/N [eV] | T [K] | Etot/N [eV] | Density [g/cm^3] | Volume [A^3] | P [bar] | Pxx [bar] | Pyy [bar] | Pzz [bar] | Pyz [bar] | Pxz [bar] | Pxy [bar]
stress = np.array([-stats[i][-6:] for i in range(stats.shape[0])])*100000 # to Pa from bar 
time = np.array([stats[i][2] for i in range(stats.shape[0])])

temperature = np.array([stats[i][5] for i in range(stats.shape[0])])
volume = np.array([stats[i][8] for i in range(stats.shape[0])])

cor = []
for i in range(3):
    c = np.real(correlate(stress[:, 3+i], stress[:, 3+i]))
    cor.append(c[0:min(len(c), args.lags)])
cor = np.array(cor)

dt = (time[1]-time[0]) * 1e-15
T = np.mean(temperature[0])
V = volume[0] * (1e-10) ** 3

kb = 1.380649e-23

eta = V / (kb * T) * integrate(np.mean(cor, axis=0), dt)

fig, ax = plt.subplots()

[ax.plot(cor[i, :]) for i in range(3)]
ax.set_ylabel("Correlation")
ax.set_xlabel("Lag, fs")
if args.show:
    plt.show()
fig.savefig(Path(out)/"cor.png")

np.savetxt(Path(out)/"stress-correlations", cor) 

print(f"{eta} Pa s")
