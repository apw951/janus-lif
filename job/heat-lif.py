from ase.build import bulk
from ase.io import write, read

from janus_core.calculations.geom_opt import optimize
from janus_core.helpers.mlip_calculators import choose_calculator
from janus_core.calculations.md import NPT, NVT
from janus_core.helpers.stats import Stats
from ase.optimize import LBFGS

import argparse

parser = argparse.ArgumentParser()

parser.add_argument("--temp_start", action="store", default="50", dest="temp_start", type=float)
parser.add_argument("--temp_end", action="store", default="1500", dest="temp_end", type=float)
parser.add_argument("--temp_steps", action="store", default="1000", dest="temp_steps", type=int)
parser.add_argument("--temp_increment", action="store", default="50", dest="temp_step", type=int)
parser.add_argument("--eq_steps", action="store", default="10000", dest="eq_steps", type=int)
args = parser.parse_args()

a = 4.026
lif = bulk("LiF", crystalstructure="rocksalt", a=a, b=a, c=a, cubic=True).repeat(4)
write("LiF.cif", lif)

lif.calc = choose_calculator(architecture="mace_mp", model="small",precision="float64",device='cuda')
print(lif.get_potential_energy())

lif_opt = optimize(lif, optimizer=LBFGS, fmax=0.001,  filter_kwargs={"hydrostatic_strain": True})

npt = NPT(struct=lif_opt,
        traj_every=1000,
        restart_every=1000,
        stats_every=100,
        equil_steps=0,
        steps=args.eq_steps,
        timestep=1.0,
        temp_start=args.temp_start,
        temp_end=args.temp_end,
        temp_step=args.temp_step,
        temp_time=args.temp_steps,
        temp=args.temp_end,
        barostat_time=100,
        thermostat_time=50,
        pressure=0.0,
        log_kwargs={"filename": "log.heating"})

npt.run()

print(f"{lif_opt.get_temperature()} K, {lif_opt.get_volume()}, {lif_opt.get_potential_energy()}") 

write("LiF-heated.cif", lif_opt)
