import argparse
from ase.io import write, read

parser = argparse.ArgumentParser(description='Convert DL_POLY CONFIG to CIF')
parser.add_argument('-config', dest='config', type=str, default='CONFIG', help='DL_POLY CONFIG')
args = parser.parse_args()

out = args.config + '.cif'

atoms = read(args.config)
write(out, atoms, format='cif')

