from ase.build import bulk
from ase.io import write, read

from janus_core.helpers.mlip_calculators import choose_calculator
from janus_core.calculations.md import NVE
from janus_core.helpers.stats import Stats

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--steps", action="store", default="100000", dest="steps", type=int)
parser.add_argument("--temp", action="store", default="1500", dest="temp", type=float)
parser.add_argument("--stats", action="store", default="100", dest="stats", type=int)
args = parser.parse_args()

lif = read("LiF-heated.cif")

lif.calc = choose_calculator(architecture="mace_mp", model="small",precision="float64",device='cuda')
print(lif.get_potential_energy())

nvt = NVE(struct=lif,
          steps=args.steps,
          traj_every=10000,
          restart_every=10000,
          stats_every=args.stats,
          equil_steps=0,
          timestep=1.0,
          temp=args.temp,
          stats_file="nve.dat")

nvt.run()

print(f"{lif.get_temperature()} K, {lif.get_volume()}, {lif.get_potential_energy()}") 
