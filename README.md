## Updating

To update janus, ```cd janus-core``` (or wherever you put it), then

```
git pull
poetry install
```

You may also need to run ```poetry lock``` and then ```poetry install``` again if it complains about the lock file
